# Hello World

![helloworld.svg](helloworld.svg)

## Description
This process is a simple BPMN process using the following elements:
- None Start Event
- Script Task: Prints hello [name] using the process variable name
- None End Event


## Generation
The web service was generated using the command `strmsrv init` in the directory where the .bpmn file is.

1. After cloning the project, run the service: `go run app\service\main.go`
2. Post a JSON payload to the start endpoint:
`http://localhost:8081/helloworldprocess/start`
```json
{
    "id": "1",
    "data": {"name": "world"}
}
```
3. The output in the terminal of the service should be "Hello world"