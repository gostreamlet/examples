package start

import (
	"context"
	"encoding/json"
	"fmt"
	"helloworld/business/processes/helloworld"
	"net/http"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

type ProcessInstanceStartRequest struct {
	Id   string
	Data helloworld.Input
}

type StartHelloWorldProcessHandler struct {
	Engine *streamlet.BPMNEngine
}

func (s StartHelloWorldProcessHandler) StartProcess(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	var startRequest ProcessInstanceStartRequest
	err := json.NewDecoder(req.Body).Decode(&startRequest)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}

	startEvent := streamlet.NewNoneStartEvent(helloworld.StartHelloWorldEvent, startRequest.Id)
	p := helloworld.NewHelloWorldProcess(startRequest.Id, s.Engine, startEvent, startRequest.Data)
	s.Engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	return nil
}
