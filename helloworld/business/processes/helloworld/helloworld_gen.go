package helloworld

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

const (
	StartHelloWorldEvent = "StartHelloWorldEvent"

	Flow_1a30jmi = "Flow_1a30jmi"

	Flow_1m2d1kw = "Flow_1m2d1kw"

	HelloWorldScript = "HelloWorldScript"

	EndHelloWorldEvent = "EndHelloWorldEvent"
)

func NewHelloWorldProcess(id string, bpmnEngine *streamlet.BPMNEngine, startElement streamlet.ElementStateProvider, input Input) *streamlet.ProcessInstance {
	impl := HelloWorldProcess{sync.RWMutex{}, &input}
	pi := streamlet.ProcessInstance{Key: "HelloWorldProcess", Id: id, BpmnEngine: bpmnEngine, Status: streamlet.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type HelloWorldProcess struct {
	mu    sync.RWMutex
	input *Input
}

func (p *HelloWorldProcess) ProcessInstanceData() any {
	return *p.input
}

func UnmarshalTask(r io.Reader) (streamlet.CompleteUserTasksCmd, error) {
	tasksCmd := streamlet.CompleteUserTasksCmd{Tasks: make([]streamlet.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := streamlet.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *streamlet.BPMNEngine, key string) T {
	states, err := engine.StateStore.GetElementStates(streamlet.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*streamlet.UserTask[T]).Data
}

func (p *HelloWorldProcess) GetNextElement(engine *streamlet.BPMNEngine, g streamlet.IdGenerator, currentElement streamlet.ElementStateProvider) streamlet.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case StartHelloWorldEvent:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(Flow_1a30jmi, id)

	case Flow_1a30jmi:
		id := g.GenerateId(streamlet.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *streamlet.BPMNEngine, token *streamlet.Token, task *streamlet.ScriptTask) {
			fmt.Printf("Hello %s\n", p.input.Name)
		}
		return streamlet.NewScriptTask(HelloWorldScript, id, script)

	case Flow_1m2d1kw:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(EndHelloWorldEvent, id)

	case HelloWorldScript:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(Flow_1m2d1kw, id)

	case EndHelloWorldEvent:
		return nil

	}
	return nil
}
