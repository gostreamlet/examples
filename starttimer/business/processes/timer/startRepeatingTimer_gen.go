package timer

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

const (
	Start = "Start"

	SequenceFlow_0ci9n6x = "SequenceFlow_0ci9n6x"

	SequenceFlow_0gawtsr = "SequenceFlow_0gawtsr"

	SequenceFlow_1k9rpkj = "SequenceFlow_1k9rpkj"

	SequenceFlow_0eltyfl = "SequenceFlow_0eltyfl"

	SequenceFlow_1s44iah = "SequenceFlow_1s44iah"

	SequenceFlow_1t71n6w = "SequenceFlow_1t71n6w"

	ParallelGateway_08mjjnp = "ParallelGateway_08mjjnp"

	ParallelGateway_0en7ysa = "ParallelGateway_0en7ysa"

	SleepScriptTask = "SleepScriptTask"

	PrintScriptTask = "PrintScriptTask"

	EndEvent = "EndEvent"
)

func NewStartTimerProcess(id string, bpmnEngine *streamlet.BPMNEngine, startElement streamlet.ElementStateProvider, timerInput TimerInput) *streamlet.ProcessInstance {
	impl := StartTimerProcess{sync.RWMutex{}, &timerInput}
	pi := streamlet.ProcessInstance{Key: "StartTimerProcess", Id: id, BpmnEngine: bpmnEngine, Status: streamlet.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type StartTimerProcess struct {
	mu         sync.RWMutex
	timerInput *TimerInput
}

func (p *StartTimerProcess) ProcessInstanceData() any {
	return *p.timerInput
}

func UnmarshalTask(r io.Reader) (streamlet.CompleteUserTasksCmd, error) {
	tasksCmd := streamlet.CompleteUserTasksCmd{Tasks: make([]streamlet.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := streamlet.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *streamlet.BPMNEngine, key string) T {
	states, err := engine.StateStore.GetElementStates(streamlet.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*streamlet.UserTask[T]).Data
}

func (p *StartTimerProcess) GetNextElement(engine *streamlet.BPMNEngine, g streamlet.IdGenerator, currentElement streamlet.ElementStateProvider) streamlet.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case Start:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_0ci9n6x, id)

	case SequenceFlow_0ci9n6x:
		id := g.GenerateId(streamlet.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0ci9n6x}
		outSeqKeys := []string{SequenceFlow_0gawtsr, SequenceFlow_1k9rpkj}
		return streamlet.NewParallelGateway(ParallelGateway_0en7ysa, id, SequenceFlow_0ci9n6x, inSeqKeys, outSeqKeys)

	case SequenceFlow_0gawtsr:
		id := g.GenerateId(streamlet.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *streamlet.BPMNEngine, token *streamlet.Token, task *streamlet.ScriptTask) {
			fmt.Printf("[%s] Script executed at: %s\n", id, time.Now())
		}
		return streamlet.NewScriptTask(PrintScriptTask, id, script)

	case SequenceFlow_1k9rpkj:
		id := g.GenerateId(streamlet.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *streamlet.BPMNEngine, token *streamlet.Token, task *streamlet.ScriptTask) {
			time.Sleep(100 * time.Millisecond)
		}
		return streamlet.NewScriptTask(SleepScriptTask, id, script)

	case SequenceFlow_0eltyfl:
		id := g.GenerateId(streamlet.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0eltyfl, SequenceFlow_1t71n6w}
		outSeqKeys := []string{SequenceFlow_1s44iah}
		return streamlet.NewParallelGateway(ParallelGateway_08mjjnp, id, SequenceFlow_0eltyfl, inSeqKeys, outSeqKeys)

	case SequenceFlow_1s44iah:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(EndEvent, id)

	case SequenceFlow_1t71n6w:
		id := g.GenerateId(streamlet.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0eltyfl, SequenceFlow_1t71n6w}
		outSeqKeys := []string{SequenceFlow_1s44iah}
		return streamlet.NewParallelGateway(ParallelGateway_08mjjnp, id, SequenceFlow_1t71n6w, inSeqKeys, outSeqKeys)

	case ParallelGateway_08mjjnp:
		return nil

	case ParallelGateway_0en7ysa:
		return nil

	case SleepScriptTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_0eltyfl, id)

	case PrintScriptTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_1t71n6w, id)

	case EndEvent:
		return nil

	}
	return nil
}
