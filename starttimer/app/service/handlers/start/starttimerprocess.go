package start

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"starttimer/business/processes/timer"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

type ProcessInstanceStartRequest struct {
	Id   string
	Data timer.TimerInput
}

type StartStartTimerProcessHandler struct {
	Engine *streamlet.BPMNEngine
}

func (s StartStartTimerProcessHandler) StartProcess(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	var startRequest ProcessInstanceStartRequest
	err := json.NewDecoder(req.Body).Decode(&startRequest)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}

	startEvent := streamlet.NewNoneStartEvent(timer.Start, startRequest.Id)
	p := timer.NewStartTimerProcess(startRequest.Id, s.Engine, startEvent, startRequest.Data)
	s.Engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	return nil
}
