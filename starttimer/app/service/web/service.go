package web

import (
	"fmt"
	"net/http"
	"starttimer/app/service/handlers"
	"starttimer/app/service/starters"

	"gitlab.com/gostreamlet/streamlet/streamlet"
	processhandlers "gitlab.com/gostreamlet/web/app/services/process-api/handlers"
	"gitlab.com/gostreamlet/web/foundation/web"
)

type Service struct {
	Engine               *streamlet.BPMNEngine
	ProcessHandlers      []processhandlers.ProcessHandler
	StartEventProcessors []streamlet.StartEventProcessor
	Mux                  *web.App
	Address              string
}

func New() *Service {
	svc := Service{Engine: streamlet.NewMemoryBPMNEngine()}

	// Start process web handlers
	svc.ProcessHandlers = handlers.GetProcessHandlers(svc.Engine)

	// Start event processors
	svc.StartEventProcessors = starters.GetStarters(svc.Engine)

	// API Mux
	cfg := processhandlers.APIMuxConfig{Engine: svc.Engine, ProcessHandlers: svc.ProcessHandlers}
	svc.Mux = processhandlers.APIMux(cfg)
	svc.Address = "localhost:8081"
	return &svc
}

func (svc *Service) Run() error {
	for i := range svc.StartEventProcessors {
		svc.Engine.AddStartEventProcesser(svc.StartEventProcessors[i])
	}
	err := svc.Engine.Start()
	if err != nil {
		return err
	}

	fmt.Printf("Listening on http://%s\n", svc.Address)
	return http.ListenAndServe(svc.Address, svc.Mux)
}
