package starters

import (
	"context"
	"starttimer/business/processes/timer"
	"time"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

type StartStarter struct {
	BpmnEngine *streamlet.BPMNEngine
}

func (t StartStarter) StartEventProcesses(ctx context.Context) {
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(5000000 * time.Microsecond):
				startEvent := streamlet.NewTimerStartEvent("Start", t.BpmnEngine.IdGenerator.GenerateId(streamlet.TimerStartEventType))
				p := timer.NewStartTimerProcess(t.BpmnEngine.IdGenerator.GenerateId(streamlet.ProcessInstanceType), t.BpmnEngine, startEvent, timer.TimerInput{})
				t.BpmnEngine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})

			}
		}
	}()
}
