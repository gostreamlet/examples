# Hello World

![startRepeatingTimer.svg](startRepeatingTimer.svg)

## Description
This process is a simple BPMN process using the following elements:
- Timer Start Event with ISO 8601 Time Cycle: R/PT5S
- Parallel Gateway
- Script Task: Sleeps for 100 milliseconds
- Script Task: Prints the time the script was executed and the element's id. 
- None End Event

## Generation
The web service was generated using the command `strmsrv init` in the directory where the .bpmn file is.

1. After cloning the project, run the service: `go run app\service\main.go`
2. The start event automatically triggers a new process every 5 seconds. 
3. The output in the terminal of the service should display the time the script ran,
approximately every 5 seconds.