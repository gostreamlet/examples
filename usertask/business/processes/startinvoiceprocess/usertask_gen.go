package startinvoiceprocess

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

const (
	StartInvoiceEvent = "StartInvoiceEvent"

	Flow_19yn8fg = "Flow_19yn8fg"

	Flow_1svkt3g = "Flow_1svkt3g"

	Flow_02sonlo = "Flow_02sonlo"

	Flow_0rxw34n = "Flow_0rxw34n"

	Flow_1y93nl1 = "Flow_1y93nl1"

	Flow_1j6dnd4 = "Flow_1j6dnd4"

	Flow_01ywhcv = "Flow_01ywhcv"

	Flow_02jr12p = "Flow_02jr12p"

	ReviewInvoiceTask = "ReviewInvoiceTask"

	Gateway_07h7z8h = "Gateway_07h7z8h"

	ApprovedExclusiveGateway = "ApprovedExclusiveGateway"

	GenerateInvoiceScript = "GenerateInvoiceScript"

	NotifyApprovalScript = "NotifyApprovalScript"

	InvoiceApprovedEvent = "InvoiceApprovedEvent"

	InvoiceAutoApprovedEvent = "InvoiceAutoApprovedEvent"

	InvoiceRejectedEvent = "InvoiceRejectedEvent"
)

func NewStartInvoiceProcess(id string, bpmnEngine *streamlet.BPMNEngine, startElement streamlet.ElementStateProvider, data Data) *streamlet.ProcessInstance {
	impl := StartInvoiceProcess{sync.RWMutex{}, &data}
	pi := streamlet.ProcessInstance{Key: "StartInvoiceProcess", Id: id, BpmnEngine: bpmnEngine, Status: streamlet.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type StartInvoiceProcess struct {
	mu   sync.RWMutex
	data *Data
}

func (p *StartInvoiceProcess) ProcessInstanceData() any {
	return *p.data
}

func UnmarshalTask(r io.Reader) (streamlet.CompleteUserTasksCmd, error) {
	tasksCmd := streamlet.CompleteUserTasksCmd{Tasks: make([]streamlet.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := streamlet.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		case ReviewInvoiceTask:
			var taskData ApproveTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		default:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *streamlet.BPMNEngine, key string) T {
	states, err := engine.StateStore.GetElementStates(streamlet.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*streamlet.UserTask[T]).Data
}

func (p *StartInvoiceProcess) GetNextElement(engine *streamlet.BPMNEngine, g streamlet.IdGenerator, currentElement streamlet.ElementStateProvider) streamlet.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case StartInvoiceEvent:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(Flow_19yn8fg, id)

	case Flow_19yn8fg:
		id := g.GenerateId(streamlet.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *streamlet.BPMNEngine, token *streamlet.Token, task *streamlet.ScriptTask) {
			p.data.invoiceAmount = 550
		}
		return streamlet.NewScriptTask(GenerateInvoiceScript, id, script)

	case Flow_1svkt3g:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if p.data.invoiceAmount >= 500 {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(Flow_02sonlo, id)
			} else {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(Flow_1y93nl1, id)
			}
		}
		return streamlet.NewExclusiveGateway(Gateway_07h7z8h, id, gwHandler)

	case Flow_02sonlo:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[ApproveTask](ReviewInvoiceTask, id)

	case Flow_0rxw34n:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if TaskData[ApproveTask](engine, ReviewInvoiceTask).Approved {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(Flow_1j6dnd4, id)
			} else {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(Flow_01ywhcv, id)
			}
		}
		return streamlet.NewExclusiveGateway(ApprovedExclusiveGateway, id, gwHandler)

	case Flow_1y93nl1:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(InvoiceAutoApprovedEvent, id)

	case Flow_1j6dnd4:
		id := g.GenerateId(streamlet.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *streamlet.BPMNEngine, token *streamlet.Token, task *streamlet.ScriptTask) {
			fmt.Println("Invoice Approved")
		}
		return streamlet.NewScriptTask(NotifyApprovalScript, id, script)

	case Flow_01ywhcv:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(InvoiceRejectedEvent, id)

	case Flow_02jr12p:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(InvoiceApprovedEvent, id)

	case ReviewInvoiceTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(Flow_0rxw34n, id)

	case Gateway_07h7z8h:
		gw := currentElement.(*streamlet.ExclusiveGateway)
		return gw.NextElement

	case ApprovedExclusiveGateway:
		gw := currentElement.(*streamlet.ExclusiveGateway)
		return gw.NextElement

	case GenerateInvoiceScript:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(Flow_1svkt3g, id)

	case NotifyApprovalScript:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(Flow_02jr12p, id)

	case InvoiceApprovedEvent:
		return nil

	case InvoiceAutoApprovedEvent:
		return nil

	case InvoiceRejectedEvent:
		return nil

	}
	return nil
}
