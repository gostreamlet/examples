package startinvoiceprocess

type Data struct {
	invoiceAmount float32
}

type ApproveTask struct {
	Approved   bool
	ApprovedBy string
}
