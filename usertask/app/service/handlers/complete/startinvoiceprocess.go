package complete

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"usertask/business/processes/startinvoiceprocess"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

type CompleteStartInvoiceProcessTaskHandler struct {
	Engine *streamlet.BPMNEngine
}

func (h CompleteStartInvoiceProcessTaskHandler) CompleteTask(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	completeReq, err := startinvoiceprocess.UnmarshalTask(req.Body)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return nil
	}
	result, err := h.Engine.SendCommand(completeReq)
	if err != nil || result == nil {
		http.Error(w, "Error completing task", 500)
	}
	var resp streamlet.CompleteTasksResp
	resp = result.(streamlet.CompleteTasksResp)
	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		http.Error(w, "Error marshalling response", 500)
		fmt.Println(err)
	}
	return nil
}
