# User Task 

![usertask.svg](usertask.svg)

## Description
This process is a simple BPMN process using the following elements:
- None Start Event 
- Script Task: Generates the invoice amount:
  `p.data.invoiceAmount = 550`
  - `p` is a variable that references the process instance
  - `data` is a field on the process that contains global process data.
  In other engines this would be called the "process variables".
  - In this process data a float32 invoiceAmount field holds the invoice value. In a more realistic example a special money type would be used.
- Exclusive Gateways
  - The first gateway checks if approval is required, defined as an invoice > $500:
  `p.data.invoiceAmount >= 500`
  - The second gateway checks if the invoice was approved by the approver:
  `TaskData[ApproveTask](engine, ReviewInvoiceTask).Approved`.
  - To access data from a task, the `TaskData[T]()` function is used. The function uses Go generics, so the type of the task needs to be passed in, in this case ApproveTask.
  The function also requires the engine to be passed in, as well as the constant for the task key to retrieve. The data
  is retrieved from the state store. The engine must be passed in currently for technical reasons.
- User Task for Invoice Review and Approval
  - To specify the go type for the task data, a `go:dataType` attribute must be set in the XML for the `bpmn:userTask`:
  `<bpmn:userTask id="ReviewInvoiceTask" name="Review Invoice" go:dataType="ApproveTask">`
  - This enables statically typed data for completing a task, as well as retrieving task data.
- None End Event

## Generation
The web service was generated using the command `strmsrv init` in the directory where the .bpmn file is.

1. After cloning the project, run the service: `go run app\service\main.go`
2. Post a JSON payload to the start endpoint:
   `http://localhost:8081/startinvoiceprocess/start`
```json
{
    "id": "1"
}
```
3. View the debug task list at `http://localhost:8081/startinvoiceprocess/debug/tasks`
4. Post a JSON payload to complete/approve the task. Replace the processInstanceId and taskId with those from the task list above.

    `http://localhost:8081/startinvoiceprocess/tasks/complete`
```json
{
    "tasks":[
        {
            "processInstanceId": "1",
            "taskKey": "ReviewInvoiceTask",
            "taskId": "6",
            "data": {
                "approved": true
            }
        }
    ]
}
```
5. "Invoice Approved" should be displayed in the console.